<?php
/**
 * WordPress Feature Support
 *
 * @package    Saved
 * @subpackage Functions
 * @copyright  Copyright (c) 2017, churchthemes.com
 * @link       https://churchthemes.com/themes/saved
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Add theme support for WordPress features
 *
 * @since 1.0
 */
function saved_add_theme_support_wp() {

	global $_wp_additional_image_sizes;

	// Output HTML5 markup
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// Title Tag
	add_theme_support( 'title-tag' );

	// RSS feeds in <head>
	add_theme_support( 'automatic-feed-links' );

	// Featured images
	add_theme_support( 'post-thumbnails' );

	// Custom Header
	add_theme_support( 'custom-header', array(
		'width'                  => $_wp_additional_image_sizes['saved-banner']['width'], // see includes/images.php
		'height'                 => $_wp_additional_image_sizes['saved-banner']['height'],
		'flex-width'             => false, // false fixes aspect ratio
		'flex-height'            => false, // false fixes aspect ratio
		'header-text'            => false,
	) );

}

add_action( 'after_setup_theme', 'saved_add_theme_support_wp' );
