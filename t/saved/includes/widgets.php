<?php
/**
 * Widget Functions
 *
 * @package    Saved
 * @subpackage Functions
 * @copyright  Copyright (c) 2017, churchthemes.com
 * @link       https://churchthemes.com/themes/saved
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**************************************
 * HOMEPAGE WIDGETS
 **************************************/

/**
 * Wrap highlight widgets in container on homepage
 *
 * This is to allow them to show consecutively in a row as a section.
 *
 * @since  1.0
 * @param  array $widget Widget data
 */
function saved_home_highlight_widget_section( $widget ) {

	global $saved_last_home_widget_is_highlight;

	// Frontend only
	// dynamic_sidebar runs in admin area too
	if ( is_admin() ) {
		return;
	}

	// In homepage widget area only
	// Otherwise, the container will occur in footer, etc.
	if ( ! ctfw_is_sidebar( 'ctcom-home' ) ) {

		// Clear out data when in other widget area
		if ( isset( $saved_last_home_widget_is_highlight ) ) {
			unset( $saved_last_home_widget_is_highlight );
		}

		// Prevent output
		return;

	}

	// Is this a highlight widget?
	$is_highlight = isset( $widget['classname'] ) && 'widget_ctfw-highlight' == $widget['classname'] ? true : false;

	// Open container
	// Last widget was not CT Highlight but this is, so we start the row
	if ( empty( $saved_last_home_widget_is_highlight ) && $is_highlight ) {
		echo '<section class="saved-home-highlights-section ' . saved_alternating_bg_class( 'contrast' ) . '">';
		echo '	<div class="saved-home-highlights-content saved-highlights-four-columns">';
	}

	// Close container if was opened
	// Last widget was CT Highlight but this is not, so we end the row
	elseif ( ! empty( $saved_last_home_widget_is_highlight ) && ! $is_highlight ) {
		echo '	</div>';
		echo '</section>';
	}

	// Record what this widget is, to be used as "last widget" on next widget
	$saved_last_home_widget_is_highlight = $is_highlight;

}

add_action( 'dynamic_sidebar', 'saved_home_highlight_widget_section' );

/**
 * Wrap secondary widgets in container on homepage
 *
 * Most widgets are designed narrow, so this allows consecutive secondary widgets to be shown
 * on the homepage in narrow columns, similar to how they would appear in footer or sidebar.
 *
 * Every widget THAT IS NOT listed below as a primary widget is considred secondary on homepage.
 *
 * - CT Section
 * - CT Highlight
 * - CT Posts
 * - CT Sermons
 * - CT Events
 * - CT Locations
 * - CT People
 * - CT Gallery
 * - CT Giving
 *
 * @since  1.0
 * @param  array $widget Widget data
 */
function saved_home_secondary_widgets_section( $widget ) {

	global $saved_last_home_widget_is_secondary;

	// Frontend only
	// dynamic_sidebar runs in admin area too
	if ( is_admin() ) {
		return;
	}

	// In homepage widget area only
	// Otherwise, the container will occur in footer, etc.
	if ( ! ctfw_is_sidebar( 'ctcom-home' ) ) {

		// Clear out data when in other widget area
		if ( isset( $saved_last_home_widget_is_secondary ) ) {
			unset( $saved_last_home_widget_is_secondary );
		}

		// Prevent output
		return;

	}

	// Is this not one of the primary widgets?
	$primary_widgets = array(
		'widget_ctfw-section',
		'widget_ctfw-highlight',
		'widget_ctfw-posts',
		'widget_ctfw-sermons',
		'widget_ctfw-events',
		'widget_ctfw-locations',
		'widget_ctfw-people',
		'widget_ctfw-gallery',
		'widget_ctfw-giving',
	);
	$is_secondary = isset( $widget['classname'] ) && ! in_array( $widget['classname'], $primary_widgets ) ? true : false;

	// Open container
	// Last widget was not secondary but this is, so we start the row
	if ( empty( $saved_last_home_widget_is_secondary ) && $is_secondary ) {
		echo '<section class="saved-home-secondary-widgets-section saved-widgets-row ' . saved_alternating_bg_class( 'contrast' ) . '">'; // contrast for widgets having white entry boxes
		echo '	<div class="saved-widgets-row-inner saved-centered-large">';
		echo '		<div class="saved-widgets-row-content">';
	}

	// Close container if was opened
	// Last widget was secondary but this is not, so we end the row
	elseif ( ! empty( $saved_last_home_widget_is_secondary ) && ! $is_secondary ) {
		echo '		</div>';
		echo '	</div>';
		echo '</section>';
	}

	// Record what this widget is, to be used as "last widget" on next widget
	$saved_last_home_widget_is_secondary = $is_secondary;

}

add_action( 'dynamic_sidebar', 'saved_home_secondary_widgets_section' );

/**
 * Force settings for Gallery widget on homepage
 *
 * @since 1.0
 * @param array $args Arguments in framework CT Gallery widget's ctfw_get_posts() method
 */
function saved_home_gallery_widget_args( $args ) {

	// Homepage widget area only
	if ( ! ctfw_is_sidebar( 'ctcom-home' ) ) {
		return $args;
	}

	// Force certain settings
	// These are hidden in admin area, for homepage widget area only
	$args['posts_per_page'] = 5; // limit
	$args['orderby'] = 'publish_date';
	$args['order'] = 'desc';

	return $args;

}

add_filter( 'ctfw_widget_gallery_get_posts_args', 'saved_home_gallery_widget_args' );

/**
 * Force settings for Posts widget on homepage
 *
 * @since 1.0
 * @param array $args Arguments in framework widget's ctfw_get_posts() method
 */
function saved_home_posts_widget_args( $args, $instance ) {

	// Homepage widget area only
	if ( ! ctfw_is_sidebar( 'ctcom-home' ) ) {
		return $args;
	}

	// Limit depends on whether or not image is shown
	$image = wp_get_attachment_image_src( $instance['image_id'] );
	$limit = empty( $image[0] ) ? 4 : 3;

	// Force certain settings
	// These are hidden in admin area, for homepage widget area only
	$args['numberposts'] = $limit; // limit
	$args['orderby'] = 'publish_date';
	$args['order'] = 'desc';

	return $args;

}

add_filter( 'ctfw_widget_posts_get_posts_args', 'saved_home_posts_widget_args', 10, 2 );

/**
 * Force settings for Sermons widget on homepage
 *
 * @since 1.0
 * @param array $args Arguments in framework widget's ctfw_get_posts() method
 */
function saved_home_sermons_widget_args( $args, $instance ) {

	// Homepage widget area only
	if ( ! ctfw_is_sidebar( 'ctcom-home' ) ) {
		return $args;
	}

	// Limit depends on whether or not image is shown
	$image = wp_get_attachment_image_src( $instance['image_id'] );
	$limit = empty( $image[0] ) ? 4 : 3;

	// Force certain settings
	// These are hidden in admin area, for homepage widget area only
	$args['numberposts'] = $limit; // limit
	$args['orderby'] = 'publish_date';
	$args['order'] = 'desc';

	return $args;

}

add_filter( 'ctfw_widget_sermons_get_posts_args', 'saved_home_sermons_widget_args', 10, 2 );

/**
 * Force settings for People widget on homepage
 *
 * @since 1.0
 * @param array $args Arguments in framework widget's ctfw_get_posts() method
 */
function saved_home_people_widget_args( $args, $instance ) {

	// Homepage widget area only
	if ( ! ctfw_is_sidebar( 'ctcom-home' ) ) {
		return $args;
	}

	// Limit depends on whether or not image is shown
	$image = wp_get_attachment_image_src( $instance['image_id'] );
	$limit = empty( $image[0] ) ? 4 : 3;

	// Force certain settings
	// These are hidden in admin area, for homepage widget area only
	$args['numberposts'] = $limit; // limit
	$args['orderby'] = 'menu_order';
	$args['order'] = 'asc';

	return $args;

}

add_filter( 'ctfw_widget_people_get_posts_args', 'saved_home_people_widget_args', 10, 2 );

/**
 * Class for homepage widget to show image on left/right
 *
 * Left and right alternate based on the prior widget's position.
 *
 * @since  1.0
 * @global string $saved_widget_image_position Prior widget's image position
 * @return string Class for left or right positioning
 */
function saved_widget_image_side_class() {

	global $saved_widget_image_position; // to retrieve prior widget's image position

	// If last instance's image was left, show on right
	if ( empty( $saved_widget_image_position ) || 'right' == $saved_widget_image_position ) {
		$saved_widget_image_position = 'left';
	}

	// If this is first instance or last was shown on right, show this on left
	else {
		$saved_widget_image_position = 'right';
	}

	// Form the class
	$class = 'saved-image-section-image-' . $saved_widget_image_position;

	// Return filtered
	return apply_filters( 'saved_widget_image_side_class', $class );

}
