<?php
/**
 * WordPress Feature Support
 *
 * @package    Exodus
 * @subpackage Functions
 * @copyright  Copyright (c) 2014, churchthemes.com
 * @link       https://churchthemes.com/themes/exodus
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @since      1.0
 */

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Add theme support for WordPress features
 *
 * @since 1.0
 */
function exodus_add_theme_support_wp() {

	// Output HTML5 markup
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// Title Tag
	add_theme_support( 'title-tag' );

	// RSS feeds in <head>
	add_theme_support( 'automatic-feed-links' );

	// Featured images
	add_theme_support( 'post-thumbnails' );

	// Admin editor stylesheet
	add_editor_style( CTFW_THEME_CSS_DIR . '/admin-editor.css' );

}

add_action( 'after_setup_theme', 'exodus_add_theme_support_wp' );
