# Copyright 2009-2017 Rocketgenius, Inc.
msgid ""
msgstr ""
"Project-Id-Version: Gravity Forms Pipe Add-On 1.0\n"
"Report-Msgid-Bugs-To: http://www.gravtiyhelp.com\n"
"POT-Creation-Date: 2017-08-07 18:47:17+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: Rocketgenius <customerservice@rocketgenius.com>\n"
"Language-Team: Rocketgenius <customerservice@rocketgenius.com>\n"
"X-Generator: Gravity Forms Build Server\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Project-Id-Version: gravityformspipe\n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-Country: United States\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Textdomain-Support: yes\n"

#: class-gf-pipe.php:227
msgid "Only one Pipe Recorder field can be added to the form."
msgstr ""

#: class-gf-pipe.php:302
msgid ""
"Pipe makes it easy to record videos on your website. Using the Gravity "
"Forms Pipe Add-On, you can add Pipe video recording to your forms. The "
"recorded video content will be available with each form submission. If you "
"don't have a Pipe account you can %ssign up for one here%s."
msgstr ""

#: class-gf-pipe.php:307
msgid ""
"You can find your account hash and API key at the top of your Pipe "
"account's %ssettings page%s."
msgstr ""

#: class-gf-pipe.php:315
msgid "Account Hash"
msgstr ""

#: class-gf-pipe.php:318
msgid "Invalid Account Hash"
msgstr ""

#: class-gf-pipe.php:323
msgid "API Key"
msgstr ""

#: class-gf-pipe.php:326
msgid "Invalid API Key"
msgstr ""

#: class-gf-pipe.php:394
msgid "Video Resolution"
msgstr ""

#: class-gf-pipe.php:410 class-gf-pipe.php:491
msgid "Width"
msgstr ""

#: class-gf-pipe.php:418 class-gf-pipe.php:497
msgid "Height"
msgstr ""

#: class-gf-pipe.php:426
msgid "Max Recording Time"
msgstr ""

#: class-gf-pipe.php:434 class-gf-pipe.php:521
msgid "Environment"
msgstr ""

#: class-gf-pipe.php:447
msgid "Add bottom menu (+30px height)"
msgstr ""

#: class-gf-pipe.php:454
msgid "Autosave videos"
msgstr ""

#: class-gf-pipe.php:461
msgid "Mirror image while recording (unreadable text)"
msgstr ""

#: class-gf-pipe.php:485
msgid "Desktop Video Resolution"
msgstr ""

#: class-gf-pipe.php:486
msgid ""
"The desired video resolution when recording from desktop devices (older "
"webcams do not support high resolutions). On mobile the video resolution "
"depends on the device."
msgstr ""

#: class-gf-pipe.php:492
msgid ""
"The width of the video recorder on desktop devices. On mobile devices the "
"width is 100%."
msgstr ""

#: class-gf-pipe.php:498
msgid ""
"The height of the video recorder (without the bottom menu) on desktop "
"devices. On mobile devices the width is 120px."
msgstr ""

#: class-gf-pipe.php:503
msgid "Maximum Recording Time"
msgstr ""

#: class-gf-pipe.php:504
msgid "Maximum recording time in seconds."
msgstr ""

#: class-gf-pipe.php:509
msgid "Autosave Videos"
msgstr ""

#: class-gf-pipe.php:510
msgid ""
"If you want to save only certain videos, you can disable the autosaving. A "
"[Save] button will be shown in Pipe's desktop video recorder that when "
"clicked will trigger the video saving, conversion and storing processes."
msgstr ""

#: class-gf-pipe.php:515
msgid "Mirror Image"
msgstr ""

#: class-gf-pipe.php:516
msgid ""
"If checked Pipe shows the webcam flipped horizontally, just like the iPhone "
"camera, in a similar way you see yourself in a mirror. With the image "
"flipped, text shown to the webcam can not be read. The final recording will "
"not be flipped/mirrored regardless of this setting."
msgstr ""

#: class-gf-pipe.php:522
msgid "The Pipe environment settings to use."
msgstr ""

#: includes/class-gf-field-pipe-recorder-incompatible.php:39
msgid ""
"Pipe Recorder field is unavailable because a valid account hash has not "
"been configured."
msgstr ""

#: includes/class-gf-field-pipe-recorder-incompatible.php:87
#: includes/class-gf-field-pipe-recorder.php:187
msgid "Pipe Recorder"
msgstr ""

#: includes/class-gf-field-pipe-recorder-incompatible.php:119
#: includes/class-gf-field-pipe-recorder.php:235
msgid "Your browser does not support the video tag."
msgstr ""

#: includes/class-gf-field-pipe-recorder-incompatible.php:121
#: includes/class-gf-field-pipe-recorder.php:237
msgid "Download Video"
msgstr ""

#: includes/class-gf-field-pipe-recorder-incompatible.php:153
#: includes/class-gf-field-pipe-recorder.php:269
msgid "Click to view"
msgstr ""

#: includes/class-gf-field-pipe-recorder.php:88
#: includes/class-gf-field-pipe-recorder.php:89
msgid "Pipe Recorder Preview"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Gravity Forms Pipe Add-On"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://www.gravityforms.com"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Record videos on your website with Pipe and add them to your Gravity Forms "
"entries."
msgstr ""

#. Author of the plugin/theme
msgid "rocketgenius"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.rocketgenius.com"
msgstr ""