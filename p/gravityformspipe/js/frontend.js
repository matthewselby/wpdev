function onSaveOk( streamName, streamDuration, userId, cameraName, micName, recorderId, audioCodec, videoCodec, fileType, videoId ) {

	// Prepare video details.
	var videoDetails = {
		thumbnail: 'https://addpipevideos.s3.amazonaws.com/' + gravityformspipe_frontend_strings.accountHash + '/' + streamName + '.jpg',
		video:     'https://addpipevideos.s3.amazonaws.com/' + gravityformspipe_frontend_strings.accountHash + '/' + streamName + '.mp4',
	};

	// Get input field.
	var inputField = jQuery( '#hdfvr-content' ).siblings( 'input[type="hidden"]' );

	// Save video URL.
	inputField.val( JSON.stringify( videoDetails ) );

}

function onVideoUploadSuccess( filename, filetype ) {

	// Prepare video URL.
	var videoDetails = {
		thumbnail: 'https://addpipevideos.s3.amazonaws.com/' + gravityformspipe_frontend_strings.accountHash + '/' + filename + '.jpg',
		video:     'https://addpipevideos.s3.amazonaws.com/' + gravityformspipe_frontend_strings.accountHash + '/' + filename + '.mp4',
	};

	// Get input field.
	var inputField = jQuery( '#hdfvr-content' ).siblings( 'input[type="hidden"]' );

	// Save video URL.
	inputField.val( JSON.stringify( videoDetails ) );

}
