<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh

/**
 * Template Name: Versus
 * Template URI: http://totalpoll.com
 * Version: 1.2.2
 * Requires: 3.0.0
 * Description: Suites any two media-based choices going against each others
 * Author: MisqTech Team
 * Author URI: http://misqtech.com
 * Category: All
 * Type: media
 */

if ( ! class_exists( 'TP_Versus_Template' ) && class_exists( 'TP_Template' ) ):

	class TP_Versus_Template extends TP_Template {
		protected $textdomain = 'tp-versus';
		protected $__FILE__ = __FILE__;

		public function assets() {
			wp_enqueue_style( 'tosrus-style', $this->asset( 'assets/css/jquery.tosrus' . ( WP_DEBUG ? '' : '.min' ) . '.css' ), array(), ( WP_DEBUG ? time() : TP_VERSION ) );
			wp_register_script( 'tosrus', $this->asset( 'assets/js/min/jquery.tosrus.js' ), 'jquery', TP_VERSION );
			wp_enqueue_script( 'tp-versus', $this->asset( 'assets/js' . ( WP_DEBUG ? '' : '/min' ) . '/main.js' ), array( 'jquery', 'tosrus' ), ( WP_DEBUG ? time() : TP_VERSION ) );
		}

		public function settings() {
			return array(
				/**
				 * Sections
				 */
				'general'    => array(
					'label' => __( 'General', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'container' => array(
							'label'  => __( 'Container', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'padding'      => array(
									'type'    => 'text',
									'label'   => __( 'Padding', $this->textdomain ),
									'default' => '1em',
								),
								'border-color' => array(
									'type'    => 'color',
									'label'   => __( 'Pagination container border', $this->textdomain ),
									'default' => '#F1F1F1',
								),
							),
						),
						'messages'  => array(
							'label'  => __( 'Messages', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFAFB',
								),
								'border'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#F5BCC8',
								),
								'color'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#F44336',
								),
							),
						),
						'question'  => array(
							'label'  => __( 'Question', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'color'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'font-size'     => array(
									'type'    => 'text',
									'label'   => __( 'Font size', $this->textdomain ),
									'default' => '1.25em',
								),
								'margin-bottom' => array(
									'type'    => 'text',
									'label'   => __( 'Margin below', $this->textdomain ),
									'default' => '2em',
								),
							),
						),
						'other'     => array(
							'label'  => __( 'Other', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'border-radius' => array(
									'type'    => 'text',
									'label'   => __( 'Border radius', $this->textdomain ),
									'default' => '0px',
								),
							),

						),
					),
				),
				'layout'     => array(
					'label' => __( 'Layout & Colors', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'choices' => array(
							'label'  => __( 'Choices', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background'          => array(
									'type'    => 'color',
									'label'   => __( 'Choices background', $this->textdomain ),
									'default' => 'transparent',
								),
								'border'              => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => 'transparent',
								),
								'shadow'              => array(
									'type'    => 'color',
									'label'   => __( 'Shadow', $this->textdomain ),
									'default' => 'rgba(0,0,0,0.1)',
								),
								'padding'             => array(
									'type'    => 'text',
									'label'   => __( 'Padding', $this->textdomain ),
									'default' => '0px',
								),
							),

						),
						'vs'      => array(
							'label'  => __( 'VS Circle', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'color'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'text'       => array(
									'type'    => 'text',
									'label'   => __( 'Text', $this->textdomain ),
									'default' => 'VS',
								),
								'border'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => 'transparent',
								),
								'shadow'     => array(
									'type'    => 'color',
									'label'   => __( 'Shadow', $this->textdomain ),
									'default' => 'rgba(0, 0, 0, 0.2)',
								),
							),

						),
						'check'   => array(
							'label'  => __( 'Check Circle', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'background:sign'      => array(
									'type'    => 'color',
									'label'   => __( 'Sign color', $this->textdomain ),
									'default' => '#5A5A5A',
								),
								'border'      => array(
									'type'    => 'color',
									'label'   => __( 'Border color', $this->textdomain ),
									'default' => 'transparent',
								),
								'shadow'     => array(
									'type'    => 'color',
									'label'   => __( 'Shadow', $this->textdomain ),
									'default' => 'rgba(0, 0, 0, 0.2)',
								),
							),

						),
						'check:selected'   => array(
							'label'  => __( 'Check circle (checked)', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#2090D0',
								),
								'background:sign'      => array(
									'type'    => 'color',
									'label'   => __( 'Sign color', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'border'      => array(
									'type'    => 'color',
									'label'   => __( 'Border color', $this->textdomain ),
									'default' => 'transparent',
								),
							),

						),
						'results' => array(
							'label'  => __( 'Results', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background'  => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FAFAFA',
								),
								'color'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#F44336',
								),
								'border'      => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#F5F5F5',
								),
								'progress-bg' => array(
									'type'    => 'color',
									'label'   => __( 'Progress bar background', $this->textdomain ),
									'default' => '#E0E0E0',
								),
							),

						),
					),
				),
				'buttons'    => array(
					'label' => __( 'Buttons', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'general' => array(
							'label'  => __( 'General', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'padding'     => array(
									'type'    => 'text',
									'label'   => __( 'Padding', $this->textdomain ),
									'default' => '0.8em',
								),
								'font-weight' => array(
									'type'    => 'text',
									'label'   => __( 'Font weight', $this->textdomain ),
									'default' => '600',
								),
								'align'       => array(
									'type'    => 'select',
									'label'   => __( 'Align', $this->textdomain ),
									'extra'   => array(
										'options' => array(
											'left'   => 'Left',
											'center' => 'Center',
											'right'  => 'Right',
										),
									),
									'default' => 'center',
								),
							),

						),
						'default' => array(
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FAFAFA',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#F5F5F5',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#F1F1F1',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#DEDEDE',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#676767',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#5A5A5A',
								),
							),

						),
						'primary' => array(
							'label'  => __( 'Primary', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#269EE3',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#2090D0',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#1A7FB9',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#106BC5',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#FFFFFF',
								),
							),

						),
					),
				),
				'typography' => array(
					'label' => __( 'Typography', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'general' => array(
							'label'  => false,
							/**
							 * Fields
							 */
							'fields' => array(
								'line-height' => array(
									'type'    => 'text',
									'label'   => __( 'Line height', $this->textdomain ),
									'default' => '1.5',
								),
								'font-family' => array(
									'type'    => 'text',
									'label'   => __( 'Font family', $this->textdomain ),
									'default' => 'inherit',
								),
								'font-size'   => array(
									'type'    => 'text',
									'label'   => __( 'Font size', $this->textdomain ),
									'default' => '14px',
								),
							),
						),
					),
				),
			);

		}
	}

	return 'TP_Versus_Template';

endif;

