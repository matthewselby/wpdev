<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>

<?php $choices = $this->poll->choices(); ?>
<?php $choice_index = 0; ?>
<?php $supported_types = array( 'video', 'audio', 'image' ); ?>

<div data-tp-choices class="totalpoll-choices">
	<?php
	foreach ( $choices as $choice ):
		if ( ! in_array( $choice['content']['type'], $supported_types ) ):
			continue;
		endif;
		?>
		<?php $choice_index ++; ?>

		<label data-tp-choice class="totalpoll-choice totalpoll-choice-<?php echo $choice['content']['type'] ?>-type" itemprop="suggestedAnswer" itemscope itemtype="http://schema.org/Answer">

			<!-- Choice Content -->
			<?php include "choices/{$choice['content']['type']}.php"; ?>

			<!-- Input Indicator -->
			<?php if ( $this->current === 'vote' || $choice['checked'] ): ?>
				<div class="totalpoll-check-indicator">
					<!-- Choice Input -->
					<?php if ( $this->current === 'vote' ) {
						echo $this->choice_input( $choice )->attribute( 'class', 'totalpoll-choice-checkbox' );
					} ?>
					<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
						<path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/>
					</svg>
				</div>
			<?php endif; ?>

			<!-- Results -->
			<?php if ( $this->current === 'results' ): ?>
				<div class="totalpoll-choice-result">
					<?php include 'results/text.php'; ?>
					<?php include 'results/bar.php'; ?>
				</div>
			<?php endif; ?>

		</label>

		<?php if ( $choice_index === 1 ): ?>
		<div class="totalpoll-separator">
			<div class="totalpoll-vs"><?php echo $this->option( 'layout', 'vs', 'text' ); ?></div>
		</div>
	<?php endif; ?>
	<?php endforeach; ?>
</div>
