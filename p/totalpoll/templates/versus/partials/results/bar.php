<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>

<div class="totalpoll-choice-progress" style="width: <?php echo $choice['votes%']; ?>%"></div>