<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>

<span class="totalpoll-choice-result-text" itemprop="text"><?php echo $this->votes($choice); ?></span>