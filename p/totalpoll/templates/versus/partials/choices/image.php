<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>

<?php $supports_full = $choice['content']['thumbnail']['url'] !== $choice['content']['image']['url'] ? true : false; ?>
<?php if($supports_full): ?>
<a href="<?php echo $supports_full ? esc_attr( $choice['content']['image']['url'] ) : '#'; ?>" class="totalpoll-choice-overlay totalpoll-choice-overlay-image"></a>
<?php endif; ?>

<img src="<?php echo esc_attr( $choice['content']['thumbnail']['url'] ); ?>">