<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>

<a href="#">
	<img src="<?php echo esc_attr( $choice['content']['thumbnail']['url'] ); ?>">
</a>