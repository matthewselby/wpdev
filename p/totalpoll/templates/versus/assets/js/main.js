jQuery(function ($) {
    var imagesTosrus = $('.totalpoll-poll-container[data-template="versus"] a.totalpoll-choice-overlay-image').tosrus({
        buttons: {
            prev: false,
            next: false
        },
        wrapper: {
            onClick: 'close'
        }
    });

    var videosTosrus = $('.totalpoll-poll-container[data-template="versus"] a.totalpoll-choice-overlay-video').tosrus({
        buttons: {
            prev: false,
            next: false
        },
        wrapper: {
            onClick: 'close'
        },
        youtube: {
            imageLink: false
        }
    });

    $(document).on('totalpoll.after.ajax', function (e, data) {
        $(imagesTosrus).remove();
        $(videosTosrus).remove();
        var imagesTosrus = $('a.totalpoll-choice-overlay-image', data.container).tosrus({
            buttons: {
                prev: false,
                next: false
            },
            wrapper: {
                onClick: 'close'
            }
        });

        var videosTosrus = $('a.totalpoll-choice-overlay-video', data.container).tosrus({
            buttons: {
                prev: false,
                next: false
            },
            wrapper: {
                onClick: 'close'
            },
            youtube: {
                imageLink: false
            }
        });
    });
});