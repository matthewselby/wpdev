<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div data-tp-choices class="totalpoll-choices">
	<?php
	$choice_index = 0;
	$threshold    = 0;
	$choices      = $this->poll->choices();
	foreach ( $choices as $choice ):
		$threshold = $threshold > $choice['votes'] ? $threshold : $choice['votes'];
	endforeach;

	foreach ( $this->poll->choices() as $choice ):
		$choice_index ++;
		?>
		<div data-tp-choice class="totalpoll-choice totalpoll-choice-<?php echo $choice['content']['type']; ?> <?php echo !empty($choice['checked']) && $this->current == 'vote' ? 'checked' : ''; ?> <?php echo $choice['votes'] >= $threshold && $this->current == 'results' ? 'totalpoll-choice-winner' : ''; ?>" itemscope  itemprop="suggestedAnswer" itemtype="http://schema.org/Answer">
			<label class="totalpoll-choice-container">
				<?php
				if ( $this->current === 'vote' && $choice['content']['type'] !== 'other' ):
					include 'vote/checkbox.php';
				endif;
				?>
				<div class="totalpoll-choice-content">
					<?php
					if ( $choice['content']['type'] !== 'html' && $choice['content']['type'] !== 'other' ):
						include 'shared/label.php';
					elseif ( $choice['content']['type'] === 'html' ):
						echo do_shortcode( $choice['content']['html'] );
					elseif ( $this->current === 'vote' && $choice['content']['type'] === 'other' ):
						include 'vote/other.php';
					endif;
					if ( $this->current === 'results' ):
						include 'results/votes.php';
					endif;
					?>
				</div>
			</label>
		</div>
		<?php
	endforeach;
	?>
</div>