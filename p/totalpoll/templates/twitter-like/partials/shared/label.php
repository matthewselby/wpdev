<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div class="totalpoll-choice-label" itemprop="text">
	<?php if ( $this->current == 'results' ): ?>
		<span class="totalpoll-choice-votes"><?php echo $choice['votes%']; ?>%</span>
	<?php endif; ?>
	
	<?php echo $choice['content']['label']; ?>
</div>