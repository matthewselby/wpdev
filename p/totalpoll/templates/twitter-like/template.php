<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh

/**
 * Template Name: Twitter-like
 * Template URI: http://totalpoll.com
 * Version: 1.0.0
 * Requires: 3.0.0
 * Description: Similar look and feel of Twitter polls
 * Author: MisqTech
 * Author URI: http://misqtech.com
 * Category: All
 * Type: text
 */

if ( ! class_exists( 'TP_Twitter_Template' ) && class_exists( 'TP_Template' ) ):

	class TP_Twitter_Template extends TP_Template {
		protected $textdomain = 'tp-twitter-like';
		protected $__FILE__ = __FILE__;

		public function settings() {
			return array(
				/**
				 * Sections
				 */
				'general'    => array(
					'label' => __( 'General', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'container' => array(
							'label'  => __( 'Container', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'border'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#E1E8ED',
								),
								'padding'    => array(
									'type'    => 'text',
									'label'   => __( 'Padding', $this->textdomain ),
									'default' => '1.5em',
								),
							),
						),
						'messages'  => array(
							'label'  => __( 'Messages', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFAFB',
								),
								'border'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#F5BCC8',
								),
								'color'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#F44336',
								),
							),
						),
						'other'     => array(
							'label'  => __( 'Other', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'per-row'       => array(
									'type'       => 'number',
									'label'      => __( 'Choices per row', $this->textdomain ),
									'default'    => '1',
									'attributes' => array(
										'min'  => 1,
										'step' => 1,
									),
								),
								'animation'     => array(
									'type'    => 'text',
									'label'   => __( 'Animation duration (ms)', $this->textdomain ),
									'default' => '1000',
								),
								'border-radius' => array(
									'type'    => 'text',
									'label'   => __( 'Border radius', $this->textdomain ),
									'default' => '6px',
								),
							),

						),
					),
				),
				'choices'    => array(
					'label' => __( 'Choices', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'default' => array(
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#F2FAFF',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#47B3F3',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#47B3F3',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#1da1f2',
								),
							),

						),
						'checked' => array(
							'label'  => __( 'Checked', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#ffffff',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#ffffff',
								),
							),

						),
					),
				),
				'buttons'    => array(
					'label' => __( 'Buttons', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'default' => array(
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#F5F5F5',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#E1E8ED',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#E1E8ED',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#E1E8ED',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => 'inherit',
								),
							),

						),
						'primary' => array(
							'label'  => __( 'Primary', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#0798EE',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#1da1f2',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#0798EE',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#FFFFFF',
								),
							),

						),
					),
				),
				'votes-bar'  => array(
					'label' => __( 'Votes bar', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'bar'  => array(
							'label'  => __( 'Bar', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'color:start'        => array(
									'type'    => 'color',
									'label'   => __( 'Start color', $this->textdomain ),
									'default' => '#E1E8ED',
								),
								'color:end'          => array(
									'type'    => 'color',
									'label'   => __( 'End color', $this->textdomain ),
									'default' => '#E1E8ED',
								),
								'color:winner:start' => array(
									'type'    => 'color',
									'label'   => __( 'Winner start color', $this->textdomain ),
									'default' => '#CCECFF',
								),
								'color:winner:end'   => array(
									'type'    => 'color',
									'label'   => __( 'Winner end color', $this->textdomain ),
									'default' => '#CCECFF',
								),
							),

						),
						'text' => array(
							'label'  => __( 'Text', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'color' => array(
									'type'    => 'color',
									'label'   => __( 'Color', $this->textdomain ),
									'default' => '#333333',
								),
							),

						),
					),
				),
				'typography' => array(
					'label' => __( 'Typography', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'general' => array(
							'label'  => false,
							/**
							 * Fields
							 */
							'fields' => array(
								'line-height' => array(
									'type'    => 'text',
									'label'   => __( 'Line height', $this->textdomain ),
									'default' => '1.5',
								),
								'font-family' => array(
									'type'    => 'text',
									'label'   => __( 'Font family', $this->textdomain ),
									'default' => 'inherit',
								),
								'font-size'   => array(
									'type'    => 'text',
									'label'   => __( 'Font size', $this->textdomain ),
									'default' => '14px',
								),
							),
						),
					),
				),
			);

		}


	}

	return 'TP_Twitter_Template';

endif;

