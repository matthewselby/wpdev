<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div data-tp-choices class="totalpoll-choices">
	<?php
	$choice_index = 0;
	$per_row      = absint( $this->option( 'general', 'other', 'per-row' ) );
	$per_row      = $per_row < 1 ? 1 : $per_row;

	foreach ( $this->poll->choices() as $choice ):
		$choice_index ++;
		$row         = ceil( $choice_index / $per_row );
		$last_in_row = $per_row === 1 || ( $choice_index % $row === 0 && $choice_index !== 1 );
		?>
		<div data-tp-choice class="totalpoll-choice totalpoll-choice-<?php echo $choice['content']['type']; ?> <?php echo $choice['checked'] && $this->current == 'vote' ? 'checked' : ''; ?> <?php echo "totalpoll-choice-row-{$row}"; ?> <?php echo $last_in_row === true ? 'totalpoll-choice-last' : ''; ?> " itemprop="suggestedAnswer" itemscope itemtype="http://schema.org/Answer">
			<label class="totalpoll-choice-container">
				<?php
				if ( $this->current === 'vote' && $choice['content']['type'] !== 'other' ):
					include 'vote/checkbox.php';
				endif;
				?>
				<div class="totalpoll-choice-content">
					<?php
					if ( $choice['content']['type'] !== 'html' && $choice['content']['type'] !== 'other' ):
						include 'shared/label.php';
					elseif ( $choice['content']['type'] === 'html' ):
						echo do_shortcode( $choice['content']['html'] );
					elseif ( $this->current === 'vote' && $choice['content']['type'] === 'other' ):
						include 'vote/other.php';
					endif;
					if ( $this->current === 'results' ):
						include 'results/votes.php';
					endif;
					?>
				</div>
			</label>
		</div>
		<?php
	endforeach;
	?>
</div>