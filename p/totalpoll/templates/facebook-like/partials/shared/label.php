<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div class="totalpoll-choice-label" itemprop="text">
	<?php echo $choice['content']['label']; ?>

	<?php if ( $this->current == 'results' ): ?>
		<span> - <?php echo $this->votes( $choice ); ?></span>
	<?php endif; ?>
</div>