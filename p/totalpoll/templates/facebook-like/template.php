<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh

/**
 * Template Name: Facebook-like
 * Template URI: http://totalpoll.com
 * Version: 1.0.0
 * Requires: 3.0.0
 * Description: Similar look and feel of Facebook polls
 * Author: MisqTech
 * Author URI: http://misqtech.com
 * Category: All
 * Type: text
 */

if ( ! class_exists( 'TP_Facebook_Template' ) && class_exists( 'TP_Template' ) ):

	class TP_Facebook_Template extends TP_Template {
		protected $textdomain = 'tp-facebook-like';
		protected $__FILE__ = __FILE__;

		public function settings() {
			return array(
				/**
				 * Sections
				 */
				'general'    => array(
					'label' => __( 'General', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'container' => array(
							'label'  => __( 'Container', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'border'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#DDDDDD',
								),
							),
						),
						'messages'  => array(
							'label'  => __( 'Messages', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFAFB',
								),
								'border'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#F5BCC8',
								),
								'color'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#F44336',
								),
							),
						),
						'other'     => array(
							'label'  => __( 'Other', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'per-row'       => array(
									'type'       => 'number',
									'label'      => __( 'Choices per row', $this->textdomain ),
									'default'    => '1',
									'attributes' => array(
										'min'  => 1,
										'step' => 1,
									),
								),
								'animation'     => array(
									'type'    => 'text',
									'label'   => __( 'Animation duration (ms)', $this->textdomain ),
									'default' => '1000',
								),
								'border-radius' => array(
									'type'    => 'text',
									'label'   => __( 'Border radius', $this->textdomain ),
									'default' => '0px',
								),
							),

						),
					),
				),
				'choices'    => array(
					'label' => __( 'Choices', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'default' => array(
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#EBEDF4',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#E5E5E5',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#AEBAD1',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => 'inherit',
								),
							),

						),
						'checked' => array(
							'label'  => __( 'Checked', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#EBEDF4',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#EBEDF4',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#AEBAD1',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#AEBAD1',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => 'inherit',
								),
							),

						),
					),
				),
				'buttons'    => array(
					'label' => __( 'Buttons', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'default' => array(
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#F5F5F5',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => 'inherit',
								),
							),

						),
						'primary' => array(
							'label'  => __( 'Primary', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'background:normal' => array(
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#4E69A2',
								),
								'background:hover'  => array(
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#3b5998',
								),
								'border:normal'     => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#4E69A2',
								),
								'border:hover'      => array(
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#3b5998',
								),
								'color:normal'      => array(
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'color:hover'       => array(
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#FFFFFF',
								),
							),

						),
					),
				),
				'votes-bar'  => array(
					'label' => __( 'Votes bar', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'bar'  => array(
							'label'  => __( 'Bar', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'border'      => array(
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#AEBAD1',
								),
								'color:start' => array(
									'type'    => 'color',
									'label'   => __( 'Start color', $this->textdomain ),
									'default' => '#EBEDF4',
								),
								'color:end'   => array(
									'type'    => 'color',
									'label'   => __( 'End color', $this->textdomain ),
									'default' => '#EBEDF4',
								),
							),

						),
						'text' => array(
							'label'  => __( 'Text', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array(
								'color' => array(
									'type'    => 'color',
									'label'   => __( 'Color', $this->textdomain ),
									'default' => '#3b5998',
								),
							),

						),
					),
				),
				'typography' => array(
					'label' => __( 'Typography', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array(
						'general' => array(
							'label'  => false,
							/**
							 * Fields
							 */
							'fields' => array(
								'line-height' => array(
									'type'    => 'text',
									'label'   => __( 'Line height', $this->textdomain ),
									'default' => '1.5',
								),
								'font-family' => array(
									'type'    => 'text',
									'label'   => __( 'Font family', $this->textdomain ),
									'default' => 'inherit',
								),
								'font-size'   => array(
									'type'    => 'text',
									'label'   => __( 'Font size', $this->textdomain ),
									'default' => '14px',
								),
							),
						),
					),
				),
			);

		}


	}

	return 'TP_Facebook_Template';

endif;

