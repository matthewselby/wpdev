<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>

<input type="text" name="totalpoll[choices][other][label]" class="totalpoll-other-field" placeholder="<?php esc_attr_e( 'Other', TP_TD ); ?>">