<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div data-tp-choices class="totalpoll-choices">
	<?php
	$choices = $this->poll->choices();

	// Check whether all choices are less than 100% (pagination case)
	$total_votes_percent = 0;
	$choices_count = 0;
	foreach( $choices as $choice ):
		$choices_count++;
		$total_votes_percent += $choice['votes%'];
		if( $choices_count > 2 ):
			break;
		endif;
	endforeach;

	$choices_count = 0;
	foreach ( $choices as $choice_index => $choice ):
		$choices_count++;

		// Abort if more than 2 choices
		if( $choices_count > 2 ) :
			continue;
		endif;

		$choice_z_index = $choice['votes%'] >= 50 ? 'z-index: 1;' : '';
		$choice_width = $total_votes_percent < 100 ? 'width: 50%' : ($choice['votes%'] > 70 ? 'width: 70%;' : ($choice['votes%'] < 30 ? 'width: 30%;' : 'width:' . $choice['votes%'] . '%;'));
		$choice_checked = isset($choice['checked']) && $choice['checked'] || $this->current == 'results' && $choices_count > 1 && !$choices[($choice_index - 1)]['checked'] ? 'checked' : '';
		?>

		<div data-tp-choice style="<?php echo $this->current == 'results' ? $choice_width . $choice_z_index : ''; ?>" class="totalpoll-choice <?php echo 'totalpoll-choice-' . $choice['content']['type'] . ' totalpoll-choice-' . ($choices_count === 1 ? 'first' : 'second') . ' ' . $choice_checked; ?>" itemprop="suggestedAnswer" itemscope itemtype="http://schema.org/Answer">
			<label class="totalpoll-choice-container">
				<div class="totalpoll-choice-content">
					<?php
					if ( $this->current === 'vote' && $choice['content']['type'] !== 'other' ):
						include 'vote/checkbox.php';
					endif;
					?>
					<?php
					if ( $choice['content']['type'] !== 'html' && $choice['content']['type'] !== 'other' ):
						include 'shared/label.php';
					elseif ( $choice['content']['type'] === 'html' ):
						echo do_shortcode( $choice['content']['html'] );
					elseif ( $this->current === 'vote' && $choice['content']['type'] === 'other' ):
						include 'vote/other.php';
					endif;
					?>
				</div>
			</label>
		</div>
		<?php
	endforeach;
	?>
</div>