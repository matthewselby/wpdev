<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div class="totalpoll-choice-label" itemprop="text">

	<?php echo $choice['content']['label']; ?>

	<?php if ( $this->current == 'results' ): ?>
		<div class="totalpoll-choice-votes"><?php echo $this->votes( $choice, true ); ?></div>
	<?php endif; ?>
</div>