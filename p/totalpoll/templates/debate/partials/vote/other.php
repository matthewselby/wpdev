<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh
?>
<div class="totalpoll-choice-label">
	<input type="text" name="totalpoll[choices][other][label]" placeholder=<?php esc_attr_e( 'Other', TP_TD ); ?>>
</div>