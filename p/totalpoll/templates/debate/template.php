<?php
if ( defined( 'ABSPATH' ) === false ) :
	exit;
endif; // Shhh

/**
 * Template Name: Debate
 * Template URI: http://totalpoll.com
 * Version: 1.0.1
 * Requires: 3.0.0
 * Description: Fancy look and breath-taking animation for two-choices based debate polls.
 * Author: MisqTech
 * Author URI: http://misqtech.com
 * Category: All
 * Type: text
 */

if ( ! class_exists( 'TP_Debate_Template' ) && class_exists( 'TP_Template' ) ):

	class TP_Debate_Template extends TP_Template {
		protected $textdomain = 'tp-debate';
		protected $__FILE__ = __FILE__;

		public function settings() {
			return array (
				/**
				 * Sections
				 */
				'general'    => array (
					'label' => __( 'General', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array (
						'container' => array (
							'label'  => __( 'Container', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'background' => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'border'     => array (
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#DDDDDD',
								),
							),
						),
						'messages'  => array (
							'label'  => __( 'Messages', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'background' => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#FFFAFB',
								),
								'border'     => array (
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#F5BCC8',
								),
								'color'      => array (
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#F44336',
								),
							),
						),
						'other'     => array (
							'label'  => __( 'Other', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'animation'     => array (
									'type'    => 'text',
									'label'   => __( 'Animation duration (ms)', $this->textdomain ),
									'default' => '1000',
								),
								'text-delay'    => array (
									'type'    => 'text',
									'label'   => __( 'Text delay (ms)', $this->textdomain ),
									'default' => '300',
								),
								'border-radius' => array (
									'type'    => 'text',
									'label'   => __( 'Border radius', $this->textdomain ),
									'default' => '4px',
								),
							),

						),
					),
				),
				'choices'    => array (
					'label' => __( 'Choices', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array (
						'default' => array (
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'background'       => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'background:hover' => array (
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#DDDDDD',
								),
								'background:checked' => array (
									'type'    => 'color',
									'label'   => __( 'Background checked', $this->textdomain ),
									'default' => '#2196F3',
								),
								'color'            => array (
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#000000',
								),
								'color:checked'            => array (
									'type'    => 'color',
									'label'   => __( 'Foreground checked', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'text-shadow'       => array (
									'type'    => 'color',
									'label'   => __( 'Text shadow', $this->textdomain ),
									'default' => 'rgba(0,0,0,0)',
								),
							),
						),
						'primary' => array (
							'label'  => __( 'Primary (Checked)', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'background' => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#2196F3',
								),
								'color'      => array (
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'text-shadow' => array (
									'type'    => 'color',
									'label'   => __( 'Text shadow', $this->textdomain ),
									'default' => 'rgba(0,0,0,0)',
								),
							),
						),
						'secondary' => array (
							'label'  => __( 'Secondary (Unchecked)', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'background' => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#E14F00',
								),
								'color'      => array (
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => '#FFFFFF',
								),
								'text-shadow' => array (
									'type'    => 'color',
									'label'   => __( 'Text shadow', $this->textdomain ),
									'default' => 'rgba(0,0,0,0)',
								),
							),
						),
					),
				),
				'buttons'    => array (
					'label' => __( 'Buttons', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array (
						'default' => array (
							'label'  => __( 'Default', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'width' => array (
									'type'    => 'text',
									'label'   => __( 'Fixed width', $this->textdomain ),
									'default' => '150px',
								),
								'background' => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'background:hover'  => array (
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#2196F3',
								),
								'border'     => array (
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'border:hover'      => array (
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#2196F3',
								),
								'color'      => array (
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'color:hover'       => array (
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#FFFFFF',
								),
							),

						),
						'primary' => array (
							'label'  => __( 'Primary', $this->textdomain ),
							/**
							 * Fields
							 */
							'fields' => array (
								'background' => array (
									'type'    => 'color',
									'label'   => __( 'Background', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'background:hover'  => array (
									'type'    => 'color',
									'label'   => __( 'Background hover', $this->textdomain ),
									'default' => '#2196F3',
								),
								'border'     => array (
									'type'    => 'color',
									'label'   => __( 'Border', $this->textdomain ),
									'default' => '#EEEEEE',
								),
								'border:hover'      => array (
									'type'    => 'color',
									'label'   => __( 'Border hover', $this->textdomain ),
									'default' => '#2196F3',
								),
								'color'      => array (
									'type'    => 'color',
									'label'   => __( 'Foreground', $this->textdomain ),
									'default' => 'inherit',
								),
								'color:hover'       => array (
									'type'    => 'color',
									'label'   => __( 'Foreground hover', $this->textdomain ),
									'default' => '#FFFFFF',
								),
							),

						),
					),
				),
				'typography' => array (
					'label' => __( 'Typography', $this->textdomain ),
					/**
					 * Tabs
					 */
					'tabs'  => array (
						'general' => array (
							'label'  => false,
							/**
							 * Fields
							 */
							'fields' => array (
								'line-height' => array (
									'type'    => 'text',
									'label'   => __( 'Line height', $this->textdomain ),
									'default' => '1.5',
								),
								'font-family' => array (
									'type'    => 'text',
									'label'   => __( 'Font family', $this->textdomain ),
									'default' => 'inherit',
								),
								'font-size'   => array (
									'type'    => 'text',
									'label'   => __( 'Font size', $this->textdomain ),
									'default' => '14px',
								),
							),
						),
					),
				),
			);

		}


	}

	return 'TP_Debate_Template';

endif;

