<div class="wp-suggested-text">
	<h2><?php esc_html_e( 'Which forms collect personal data?', Forminator::DOMAIN ); ?></h2>
	<p class="privacy-policy-tutorial">
		<?php _e( 'If you use Forminator PRO to create and embed any forms on your website, you may need to mention it here to properly distinguish it from other forms.',
		                  Forminator::DOMAIN ); ?>
	</p>

	<h2><?php esc_html_e( 'What personal data do we collect and why?', Forminator::DOMAIN ); ?></h2>
	<p class="privacy-policy-tutorial">
		<?php _e( 'By default Forminator captures the <strong>IP Address</strong> for each submission to a Form. Other personal data such as your <strong>name</strong> and <strong>email address</strong> may also be captured,
depending on the Form
Fields.',
		                  Forminator::DOMAIN ); ?>
	</p>
	<p class="privacy-policy-tutorial">
		<i><?php _e( 'Note: In this section you should include any personal data you collected and which form captures personal data to give users more relevant information. You should also include an explanation of why this data is needed. The explanation must note either the legal basis for your data collection and retention of the active consent the user has given.',
		             Forminator::DOMAIN ); ?></i>
	</p>
	<p>
		<strong class="privacy-policy-tutorial"><?php esc_html_e( 'Suggested text: ', Forminator::DOMAIN ); ?></strong>
		<?php _e( 'When visitors or users submit a form, we capture the <strong>IP Address</strong> for spam protection. We also capture the <strong>email address</strong> and might capture other personal data included in the Form fields.',
		                  Forminator::DOMAIN ); ?>
	</p>

	<h2><?php esc_html_e( 'How long we retain your data', Forminator::DOMAIN ); ?></h2>
	<p class="privacy-policy-tutorial">
		<?php _e( 'By default Forminator retains all form submissions <strong>forever</strong>. You can change this setting in <strong>Forminator</strong> &raquo; <strong>Settings</strong> &raquo;
		<strong>Privacy Settings</strong>',
		                  Forminator::DOMAIN ); ?>
	</p>
	<p>
		<strong class="privacy-policy-tutorial"><?php esc_html_e( 'Suggested text: ', Forminator::DOMAIN ); ?></strong>
		<?php _e( 'When visitors or users submit a form we retain the data for 30 days.', Forminator::DOMAIN ); ?>
	</p>
	<h2><?php esc_html_e( 'Where we send your data', Forminator::DOMAIN ); ?></h2>
	<p>
		<strong class="privacy-policy-tutorial"><?php esc_html_e( 'Suggested text: ', Forminator::DOMAIN ); ?></strong>
		<?php _e( 'All collected data might be shown publicly and we send it to our workers or contractors to perform necessary actions based on the form submission.', Forminator::DOMAIN ); ?>
	</p>
	<h2><?php esc_html_e( 'Third Parties', Forminator::DOMAIN ); ?></h2>
	<p class="privacy-policy-tutorial">
		<?php _e( 'If your forms utilize either built-in or external third party services, in this section you should mention any third parties and its privacy policy.',
		                  Forminator::DOMAIN ); ?>
	</p>
	<p>
		<strong class="privacy-policy-tutorial"><?php esc_html_e( 'Suggested text: ', Forminator::DOMAIN ); ?></strong>
		<?php _e( 'We use Google Recaptcha for spam protection. Their privacy policy can be found here : https://policies.google.com/privacy?hl=en.', Forminator::DOMAIN ); ?><br/>
		<?php _e( 'We use Akismet Spam for spam protection. Their privacy policy can be found here : https://automattic.com/privacy/.', Forminator::DOMAIN ); ?>
	</p>
</div>
