<div class="wpmudev-header--main">

	<h1><?php _e( "Dashboard", Forminator::DOMAIN ); ?></h1>

</div>

<div class="wpmudev-header--sub">

	<a href="https://premium.wpmudev.org/docs/wpmu-dev-plugins/forminator/#chapter-1" target="_blank" class="wpmudev-button wpmudev-button-ghost wpmudev-button-sm"><i class="wpdui-icon wpdui-icon-academy"></i><?php _e( "View Documentation", Forminator::DOMAIN ); ?></a>

</div>