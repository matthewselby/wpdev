<div class="wpmudev-header--main">

	<h1><?php _e( "Quizzes", Forminator::DOMAIN ); ?></h1>

	<button class="wpmudev-button wpmudev-button-sm wpmudev-button-ghost wpmudev-button-open-modal" data-modal="quizzes"><?php _e( "New Quiz", Forminator::DOMAIN ); ?></button>

</div>

<div class="wpmudev-header--sub">

	<a href="https://premium.wpmudev.org/docs/wpmu-dev-plugins/forminator/#chapter-4" target="_blank" class="wpmudev-button wpmudev-button-ghost wpmudev-button-sm"><i class="wpdui-icon wpdui-icon-academy"></i><?php _e( "View Documentation", Forminator::DOMAIN ); ?></a>

</div>