<section id="wpmudev-section">

    <div class="wpmudev-row">

        <div class="wpmudev-col col-12 col-md-6">

			<?php $this->template( 'settings/widget-listings' ); ?>

		</div>

		<div class="wpmudev-col col-12 col-md-6">

			<?php $this->template( 'settings/widget-entries' ); ?>

		</div>

	</div>

	<div class="wpmudev-row">

        <div class="wpmudev-col col-12 col-md-6">

			<?php $this->template( 'settings/widget-captcha' ); ?>

		</div>

		<div class="wpmudev-col col-12 col-md-6">

			<?php $this->template( 'settings/widget-docs' ); ?>

		</div>

    </div>

	<div class="wpmudev-row">

		<div class="wpmudev-col col-12 col-md-6">

			<?php $this->template( 'settings/widget-privacy' ); ?>

		</div>

        <div class="wpmudev-col col-12 col-md-6">

			<?php $this->template( 'settings/widget-uninstall' ); ?>

		</div>

    </div>

</section>