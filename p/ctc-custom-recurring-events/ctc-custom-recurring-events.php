<?php
/**
 * Plugin Name: Church Content - Custom Recurring Events
 * Plugin URI: https://churchthemes.com/plugins/custom-recurring-events
 * Description: Adds custom recurring event options such as "every two weeks" and "third Sunday of every month" to the <a href="https://churchthemes.com/plugins/church-content/">Church Content</a> plugin.
 * Version: 1.0.9
 * Author: churchthemes.com
 * Author URI: https://churchthemes.com
 * License: GPLv2 or later
 * Text Domain: ctc-custom-recurring-events
 * Domain Path: /languages
 *
 * @package   CTC_Custom_Recurring_Events
 * @copyright Copyright (c) 2014 - 2017, churchthemes.com
 * @link      https://churchthemes.com/plugins/custom-recurring-events
 * @license   GPLv2 or later
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main class
 *
 * This defines constants then checks if the Church Content plugin is active.
 * It then enables the add-on by registering it with CTC (for licensing/updates), loading the
 * language file and including files as needed globally or for admin end only.
 *
 * @since 1.0
 */
class CTC_Custom_Recurring_Events {

	/**
	 * Plugin data from get_plugins()
	 *
	 * @since 1.0
	 * @var object
	 */
	public $plugin_data;

	/**
	 * Includes to load
	 *
	 * @since 1.0
	 * @var array
	 */
	public $includes;

	/**
	 * Constructor
	 *
	 * Add actions for methods that define constants and load includes.
	 *
	 * @since 1.0
	 * @access public
	 */
	public function __construct() {

		// Set plugin data.
		add_action( 'plugins_loaded', array( &$this, 'set_plugin_data' ), 1 );

		// Define constants.
		add_action( 'plugins_loaded', array( &$this, 'define_constants' ), 1 );

		// Enable addon if Church Content plugin is active.
		// If not, show an admin notice.
		add_action( 'plugins_loaded', array( &$this, 'enable_addon' ), 1 );

	}

	/**
	 * Set plugin data
	 *
	 * This data is used by constants.
	 *
	 * @since 1.0
	 * @access public
	 */
	public function set_plugin_data() {

		// Load plugin.php if get_plugins() not available.
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		// Get path to plugin's directory.
		$plugin_dir = plugin_basename( dirname( __FILE__ ) );

		// Get plugin data.
		$plugin_data = current( get_plugins( '/' . $plugin_dir ) );

		// Set plugin data.
		$this->plugin_data = apply_filters( 'ctc_cre_plugin_data', $plugin_data );

	}

	/**
	 * Define constants
	 *
	 * @since 1.0
	 * @access public
	 */
	public function define_constants() {

		// Add-on details.
		define( 'CTC_CRE_VERSION',           $this->plugin_data['Version'] );                               // plugin version.
		define( 'CTC_CRE_NAME',              $this->plugin_data['Name'] );                                  // plugin name.
		define( 'CTC_CRE_NAME_SHORT',        str_replace( 'Church Content - ', '', CTC_CRE_NAME ) ); 		// plugin name shortened.
		define( 'CTC_CRE_INFO_URL',          $this->plugin_data['PluginURI'] );                             // plugin's info page URL.
		define( 'CTC_CRE_FILE',              __FILE__ );                                                    // plugin's info page URL.
		define( 'CTC_CRE_FILE_BASE',         plugin_basename( CTC_CRE_FILE ) );                             // plugin's main file path.
		define( 'CTC_CRE_DIR',               dirname( CTC_CRE_FILE_BASE ) );                                // plugin's directory.
		define( 'CTC_CRE_PATH',              untrailingslashit( plugin_dir_path( CTC_CRE_FILE ) ) );        // plugin's directory.
		define( 'CTC_CRE_URL',               untrailingslashit( plugin_dir_url( CTC_CRE_FILE ) ) );         // plugin's directory URL.

		// CTC plugin details.
		define( 'CTC_CRE_BASE_PLUGIN_FILE',  'church-theme-content/church-theme-content.php' );             // Church Content plugin's main file.
		define( 'CTC_CRE_BASE_PLUGIN_DIR',   dirname( CTC_CRE_BASE_PLUGIN_FILE ) );                         // Church Content plugin's slug.

		// Directories.
		define( 'CTC_CRE_INC_DIR',           'includes' );                                                  // includes directory.
		define( 'CTC_CRE_ADMIN_DIR',         CTC_CRE_INC_DIR . '/admin' );                                  // admin directory.
		define( 'CTC_CRE_CLASS_DIR',         CTC_CRE_INC_DIR . '/classes' );                                // classes directory.
		define( 'CTC_CRE_LIB_DIR',           CTC_CRE_INC_DIR . '/libraries' );                              // libraries directory.
		define( 'CTC_CRE_CSS_DIR',           'css' );                                                       // stylesheets directory.
		define( 'CTC_CRE_JS_DIR',            'js' );                                                        // JavaScript directory.
		define( 'CTC_CRE_IMG_DIR',           'images' );                                                    // images directory.
		define( 'CTC_CRE_LANG_DIR',          'languages' );                                                 // languages directory.

	}

	/**
	 * Check if Church Content plugin is installed and has been activated
	 *
	 * @since 1.0
	 * @access public
	 * @return bool True if plugin installed and active
	 */
	public function is_ctc_active() {

		$active = false;

		if ( is_plugin_active( CTC_CRE_BASE_PLUGIN_FILE ) ) {
			$active = true;
		}

		return $active;

	}

	/**
	 * Check if Church Content plugin is installed but not necessarily active
	 *
	 * @since 1.0
	 * @access public
	 * @return bool True if plugin is installed
	 */
	public function is_ctc_installed() {

		$installed = false;

		if ( array_key_exists( CTC_CRE_BASE_PLUGIN_FILE, get_plugins() ) ) {
			$installed = true;
		}

		return $installed;

	}

	/**
	 * Check if Church Content plugin is old version
	 *
	 * Version 1.2 of the plugin is required for this add-on to work
	 *
	 * @since 1.0
	 * @access public
	 * @return bool True if plugin is older than required version
	 */
	public function is_ctc_old() {

		$old = false;

		if ( version_compare( CTC_VERSION, '1.2', '<' ) ) {
			$old = true;
		}

		return $old;

	}

	/**
	 * Enable addon if Church Content plugin is active and required version.
	 *
	 * If not, show an admin notice.
	 *
	 * @since 1.0
	 * @access public
	 */
	public function enable_addon() {

		// Is Church Content plugin installed and active?
		if ( $this->is_ctc_active() ) {

			// Is Church Content version older than required?
			if ( $this->is_ctc_old() ) {
				add_action( 'admin_notices', array( &$this, 'ctc_old_notice' ) );
			}

			// Church Content requirements are satisfied.
			// Enable this add-on.
			else {

				// Register add-on with Church Content plugin.
				add_action( 'plugins_loaded', array( &$this, 'register' ), 2 );

				// Load language file.
				add_action( 'plugins_loaded', array( &$this, 'load_textdomain' ), 2 );

				// Set includes.
				add_action( 'plugins_loaded', array( &$this, 'set_includes' ), 2 );

				// Load includes.
				add_action( 'plugins_loaded', array( &$this, 'load_includes' ), 2 );

			}

		}

		// Show admin notice if plugin needs to be installed or activated.
		else {
			add_action( 'admin_notices', array( &$this, 'ctc_install_notice' ) ); // a theme may have a priority lower than default 10.
		}

	}

	/**
	 * Show admin notice when Church Content plugin is not active
	 *
	 * If plugin is not installed or installed but inactive, the notice shows
	 *
	 * @since 1.0
	 * @access public
	 */
	public function ctc_install_notice() {

		// Show only on relevant pages as not to overwhelm the admin.
		$screen = get_current_screen();
		if ( ! in_array( $screen->base, array( 'dashboard', 'plugins' ), true ) ) {
			return;
		}

		// Prevent other add-ons or theme from showing similar notice.
		// Make sure this is after all other returns above, meaning a notice is actually showing.
		if ( ! empty( $GLOBALS['ctc_install_notice_sent'] ) ) {
			return;
		} else {
			$GLOBALS['ctc_install_notice_sent'] = true;
		}

		// Church Content plugin not installed.
		if ( ! $this->is_ctc_installed() && current_user_can( 'install_plugins' ) ) {

			$notice = sprintf(
				/* translators: %1$s is URL to install plugin */
				__( '<b>Plugin Required:</b> Please install the <a href="%1$s">Church Content</a> plugin to use the <b>%2$s</b> add-on.', 'ctc-custom-recurring-events' ),
				network_admin_url( 'plugin-install.php?s=' . rawurlencode( '"Church Content" churchthemes.com' ) . '&tab=search' ),
				CTC_CRE_NAME_SHORT
			);

		}

		// Church Content plugin installed but not activated.
		elseif ( ! $this->is_ctc_active() && current_user_can( 'activate_plugins' ) ) {

			$notice = sprintf(
				/* translators: %1$s is URL to activate plugin, %2$s is add-on name */
				__( 'Please <a href="%1$s">activate</a> the <b>Church Content</b> plugin to use the <b>%2$s</b> add-on.', 'ctc-custom-recurring-events' ),
				wp_nonce_url( self_admin_url( 'plugins.php?action=activate&plugin=' . CTC_CRE_BASE_PLUGIN_FILE ), 'activate-plugin_' . CTC_CRE_BASE_PLUGIN_FILE ),
				CTC_CRE_NAME_SHORT
			);

		}

		// Show notice.
		if ( isset( $notice ) ) {

			?>
			<div class="error">
				<p>
					<?php
					echo wp_kses(
						$notice,
						array(
							'b' => array(),
							'a' => array(
								'href' => array(),
								'class' => array(),
							),
						)
					);
					?>
				</p>
			</div>
			<?php

		}

	}

	/**
	 * Show admin notice when Church Content plugin version is old
	 *
	 * Version 1.2 is required for this add-on to work
	 *
	 * @since 1.0
	 * @access public
	 */
	public function ctc_old_notice() {

		// Show only on relevant pages as not to overwhelm the admin.
		$screen = get_current_screen();
		if ( ! in_array( $screen->base, array( 'dashboard', 'plugins' ), true ) ) {
			return;
		}

		// Prevent other add-ons or theme from showing similar notice.
		// Make sure this is after all other returns above, meaning a notice is actually showing.
		if ( ! empty( $GLOBALS['ctc_old_notice_sent'] ) ) {
			return;
		} else {
			$GLOBALS['ctc_old_notice_sent'] = true;
		}

		// Church Content plugin is old.
		if ( $this->is_ctc_old() && current_user_can( 'update_plugins' ) ) {

			$notice = sprintf(
				/* translators: %1$s is URL to update, %2$s is add-on name */
				__( '<b>Plugin Update Required:</b> Please <a href="%1$s">update</a> the <strong>Church Content</strong> plugin to the latest version to use the <b>%2$s</b> add-on.', 'ctc-custom-recurring-events' ),
				network_admin_url( 'update-core.php' ),
				CTC_CRE_NAME_SHORT
			);

		}

		// Show notice.
		if ( isset( $notice ) ) {

			?>
			<div class="notice notice-warning">
				<p>
					<?php
					echo wp_kses(
						$notice,
						array(
							'b' => array(),
							'strong' => array(),
							'a' => array(
								'href' => array(),
							),
						)
					);
					?>
				</p>
			</div>
			<?php

		}

	}

	/**
	 * Register add-on with Church Content plugin
	 *
	 * This enables several features powered by CTC and EDD Software Licensing:
	 *
	 * 1. License Key field and ability to activate/deactivate appears in Settings
	 * 2. Receive one-click updates via a store using Easy Digital Downloads Software Licensing
	 * 3. Notice appear when add-on license is inactive, expiring soon or expired
	 *
	 * @since 1.2
	 * @access public
	 */
	public function register() {

		// Ensure function is available to use.
		if ( function_exists( 'ctc_register_add_on' ) ) {

			// Register it.
			// CTC escapes translation strings below for security.
			ctc_register_add_on( array(
				'plugin_file'              => __FILE__, // plugin-name/plugin-name.php or __FILE__ if this code is in main file.
				'store_url'                => 'https://churchthemes.com', // URL of store running EDD with Software Licensing extension.
				'renewal_url'              => 'https://churchthemes.com/renew/?license_key={license_key}', // Optional URL for renewal links (ie. EDD checkout); {license_key} will be replaced with key.
				'renewal_info_url'         => 'https://churchthemes.com/go/license-renewal', // Optional URL for renewal information.
				'changelog_url'            => 'https://churchthemes.com/go/plugin-changelog/custom-recurring-events', // Optional URL for external changelog.
				'activate_fail_notice'     => sprintf( // shown when activation fails (add link to License Keys guide).
					/* translators: %1$s is URL to license keys guide */
					__( '<b>License key could not be activated.</b> Read the <a href="%1$s" target="_blank">License Keys</a> guide for help.', 'ctc-custom-recurring-events' ),
					'https://churchthemes.com/go/license-inactive'
				),
										   /* translators: %1$s is URL to add-on license settings, %2$s is name of add-on, %3$s is expiration date, %4$s is renewal URL, %5$s is renewal info URL */
				'expired_notice'           => __( '<strong>Add-on License Expired:</strong> <a href="%4$s" target="_blank">Renew</a> your <a href="%1$s">Add-on License</a> for <strong>%2$s</strong> to re-enable updates (expired on <strong>%3$s</strong>). <a href="%5$s" target="_blank">Learn More</a>', 'ctc-custom-recurring-events' ), // Add direct renewal link and "Learn More" (different notice shows when for multiple add-ons).
										   /* translators: %1$s is URL to add-on license settings, %2$s is name of add-on, %3$s is expiration date, %4$s is renewal URL, %5$s is renewal info URL */
				'expiring_soon_notice'     => __( '<strong>Add-on License Expiring Soon:</strong> <a href="%4$s" target="_blank">Renew</a> your <a href="%1$s">Add-on License</a> for <strong>%2$s</strong> to continue receiving updates (expires on <strong>%3$s</strong>). <a href="%5$s" target="_blank">Learn More</a>', 'ctc-custom-recurring-events' ), // Add direct renewal link and "Learn More" (different notice shows when for multiple add-ons).
			) );

		}

	}

	/**
	 * Load language file
	 *
	 * This will load the MO file for the current locale.
	 * The translation file must be named ctc-custom-recurring-events-$locale.mo.
	 *
	 * First it will check to see if the MO file exists in wp-content/languages/plugins.
	 * If not, then the 'languages' direcory inside the plugin will be used.
	 * It is ideal to keep translation files outside of the plugin to avoid loss during updates.
	 *
	 * @since 1.0
	 * @access public
	 */
	public function load_textdomain() {

		// Textdomain.
		$domain = 'ctc-custom-recurring-events';

		// WordPress core locale filter.
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		// WordPress 3.6 and earlier don't auto-load from wp-content/languages, so check and load manually.
		// http://core.trac.wordpress.org/changeset/22346.
		$external_mofile = WP_LANG_DIR . '/plugins/' . $domain . '-' . $locale . '.mo';
		if ( get_bloginfo( 'version' ) <= 3.6 && file_exists( $external_mofile ) ) { // external translation exists.
			load_textdomain( $domain, $external_mofile );
		}

		// Load normally.
		// Either using WordPress 3.7+ or older version with external translation.
		else {
			$languages_dir = CTC_CRE_DIR . '/' . trailingslashit( CTC_CRE_LANG_DIR ); // ensure trailing slash.
			load_plugin_textdomain( $domain, false, $languages_dir );
		}

	}

	/**
	 * Set includes
	 *
	 * @since 1.0
	 * @access public
	 */
	public function set_includes() {

		$this->includes = apply_filters( 'ctc_cre_includes', array(

			// Frontend or admin.
			'always' => array(

				// Functions.
				CTC_CRE_INC_DIR . '/helpers.php',
				CTC_CRE_INC_DIR . '/schedule.php',
				CTC_CRE_INC_DIR . '/support.php',

			),

			// Admin only.
			'admin' => array(

				// Functions.
				CTC_CRE_ADMIN_DIR . '/admin-enqueue-scripts.php',
				CTC_CRE_ADMIN_DIR . '/admin-event-fields.php',
				CTC_CRE_ADMIN_DIR . '/admin-support.php',
				CTC_CRE_ADMIN_DIR . '/import.php',
				CTC_CRE_ADMIN_DIR . '/upgrade.php',

			),

			// Frontend only.
			'frontend' => array(),

		) );

	}

	/**
	 * Load includes
	 *
	 * Include files based on whether or not condition is met.
	 *
	 * @since 1.0
	 * @access public
	 */
	public function load_includes() {

		// Get includes.
		$includes = $this->includes;

		// Loop conditions.
		foreach ( $includes as $condition => $files ) {

			$do_includes = false;

			// Check condition.
			switch ( $condition ) {

				// Admin Only.
				case 'admin':
					if ( is_admin() ) {
						$do_includes = true;
					}
					break;

				// Frontend Only.
				case 'frontend':
					if ( ! is_admin() ) {
						$do_includes = true;
					}
					break;

				// Admin or Frontend (always).
				default:
					$do_includes = true;
					break;

			}

			// Loop files if condition met.
			if ( $do_includes ) {

				foreach ( $files as $file ) {
					require_once trailingslashit( CTC_CRE_PATH ) . $file;
				}

			}

		}

	}

}

// Instantiate the main class.
new CTC_Custom_Recurring_Events();
