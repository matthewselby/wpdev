/**
 * Admin Events
 */

// DOM is fully loaded
jQuery( document ).ready( function( $ ) {

	/********************************************
	 * FIELDS
	 ********************************************/

	// Add day of week to monthly recurrence on week dropdown
	// Do this on page load and when date is changed
	ctc_cre_add_day_to_week(); // on page load
	$( '#ctmb-field-_ctc_event_start_date select, #ctmb-field-_ctc_event_start_date input' ).bind( 'change keyup', ctc_cre_add_day_to_week );

} );

// Add day of week to monthly recurrence on week dropdown
// Do this on page load and when date is changed
function ctc_cre_add_day_to_week() {

	var start_date_year, start_date_month, start_date_day, start_date, valid_date, day_of_week_num, day_of_week;

	// Show or update Start Date's day of week after week of month dropdown
	start_date_month = jQuery( '#ctmb-input-_ctc_event_start_date-month' ).val();
	start_date_year = jQuery( '#ctmb-input-_ctc_event_start_date-year' ).val();
	start_date_day = jQuery( '#ctmb-input-_ctc_event_start_date-day' ).val();
	valid_date = ctc_cre_checkdate( start_date_month, start_date_day, start_date_year );

	// Store unmodified option text before week day is appended
	jQuery( '#ctmb-input-_ctc_event_recurrence_monthly_week option' ).each( function() {
		if ( ! jQuery( this ).attr( 'data-ctc-text' ) ) {
			jQuery( this ).attr( 'data-ctc-text', jQuery( this ).text() );
		}
	} );

	// Get day of week
	if ( valid_date ) {
		start_date = new Date( start_date_year, start_date_month - 1, start_date_day ); // Months are 0 - 11
		day_of_week_num = start_date.getDay();
		day_of_week = ctc_events.week_days[ day_of_week_num ];
	}

	// Show it after select option
	jQuery( '#ctmb-input-_ctc_event_recurrence_monthly_week option' ).each( function() {

		// Pass over "Select a Week"
		if ( jQuery( this ).val() && 'none' != jQuery( this ).val() ) {

			// Add day of week to option
			if ( valid_date ) {
				jQuery( this ).text(
					ctc_events.week_of_month_format
						.replace( '\{week\}', jQuery( this ).attr( 'data-ctc-text' ) ) // First, Third, etc.
						.replace( '\{day\}', day_of_week ) // localized Sunday, Monday, etc.
				);
			} else { // if no valid date, return to original state
				jQuery( this ).text( jQuery( this ).attr( 'data-ctc-text' ) );
			}

		}

	} );

}

// Check for valid date
// From http://phpjs.org/functions/checkdate/ (MIT License)
// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// improved by: Pyerre
// improved by: Theriault
function ctc_cre_checkdate( m, d, y ) {
	return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= ( new Date( y, m, 0 ) ).getDate();
}
