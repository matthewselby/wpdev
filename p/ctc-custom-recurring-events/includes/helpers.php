<?php
/**
 * Helper Functions
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Functions
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*************************************************
 * ARRAYS
 *************************************************/

/**
 * Merge an array into another array after a specific key
 *
 * Meant for one dimensional associative arrays.
 * Can be used to insert an array of meta box fields after a specific field
 *
 * @since 1.0
 * @param array $original_array Array to merge another into.
 * @param array $insert_array Array to merge into original.
 * @param mixed $after_key Key in original array to merge second array after.
 * @return array Modified array
 */
function ctc_cre_array_merge_after_key( $original_array, $insert_array, $after_key ) {

	$modified_array = array();

	// loop original array items.
	foreach ( $original_array as $item_key => $item_value ) {

		// rebuild the array one item at a time.
		$modified_array[ $item_key ] = $item_value;

		// insert array after specific key.
		if ( $item_key === $after_key ) {
			$modified_array = array_merge( $modified_array, $insert_array );
		}

	}

	return apply_filters( 'ctc_cre_array_merge_after_key', $modified_array, $original_array, $insert_array, $after_key );

}

/*************************************************
 * DATES
 *************************************************/

/**
 * Localized days of week
 *
 * @since 1.0
 * @return array Array of days of week with 0 - 6 as keys and Sunday - Saturday translated as values
 */
function ctc_cre_week_days() {

	global $wp_locale;

	$week_days = array();

	for ( $day = 0; $day < 7; $day++ ) {
		$week_days[ $day ] = $wp_locale->get_weekday( $day );
	}

	return apply_filters( 'ctc_cre_week_days', $week_days );

}

/*************************************************
 * FUNCTIONS
 *************************************************/

/**
 * Check if a function is available
 *
 * This is helpful: http://bit.ly/100BpPJ
 *
 * @since 1.0
 * @param string $function Name of function to check.
 * @return bool True if function exists and is not disabled.
 */
function ctc_cre_function_available( $function ) {

	$available = false;

	// Function exists?
	if ( function_exists( $function ) ) {

		// Is it not disabled in php.ini?
		$disabled_functions = explode( ',', ini_get( 'disable_functions' ) );
		if ( ! in_array( $function, $disabled_functions, true ) ) {
			$available = true;
		}

	}

	return apply_filters( 'ctc_cre_function_available', $available, $function );

}
