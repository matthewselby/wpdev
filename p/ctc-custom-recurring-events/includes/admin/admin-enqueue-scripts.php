<?php
/**
 * Admin JavaScript
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Admin
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Admin JavaScript
 *
 * @since 1.0
 */
function ctc_cre_admin_enqueue_scripts() {

	$screen = get_current_screen();

	// Event Add / Edit.
	if ( 'post' === $screen->base && 'ctc_event' === $screen->post_type ) { // don't enqueue unless needed.

		// Enqueue JS.
		wp_enqueue_script( 'ctc-admin-events', CTC_CRE_URL . '/' . CTC_CRE_JS_DIR . '/admin-events.js', array( 'jquery' ), CTC_CRE_VERSION ); // bust cache on update.

		// Pass data to JS.
		wp_localize_script( 'ctc-admin-events', 'ctc_events', array(
			'week_days'            => ctc_cre_week_days(), // to show translated week day after "On a specific week" dropdown.
			/* translators: These are replacement tags. Do not translate, only change order if necessary (example result: "First Friday" or "Third Tuesday"). */
			'week_of_month_format' => esc_html_x( '{week} {day}', 'week of month', 'ctc-custom-recurring-events' ),
		) );

	}

}

add_action( 'admin_enqueue_scripts', 'ctc_cre_admin_enqueue_scripts' ); // admin-end only.
