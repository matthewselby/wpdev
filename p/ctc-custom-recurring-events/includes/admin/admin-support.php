<?php
/**
 * Admin Feature Support
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Admin
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*********************************************
 * ADMIN NOTICE
 *********************************************/

/**
 * Show admin notice when Recurrence field not supported by theme
 *
 * This is very unlikely but would clue the user in and encourage theme author to support it
 *
 * @since 1.0
 * @access public
 */
function ctc_cre_unsupported_admin_notice() {

	// Show only on relevant pages as not to overwhelm the admin.
	$screen = get_current_screen();
	if (
		! in_array( $screen->base, array( 'dashboard', 'plugins' ), true )
		&& ! ( 'post' === $screen->base && 'ctc_event' === $screen->post_type ) // Add or Edit Event.
	) {
		return;
	}

	// Show only when Recurrence field not supported.
	if ( ctc_field_supported( 'events', '_ctc_event_recurrence' ) ) {
		return;
	}

	// Show notice.
	?>
	<div class="notice notice-error">
		<p>

			<?php

			printf(
				wp_kses(
					/* translators: %1$s is add-on name short */
					__( '<b>%1$s</b> cannot be used because the current theme does not support the <b>Church Content</b> plugin\'s <i>Recurrence</i> field.', 'ctc-custom-recurring-events' ),
					array(
						'b' => array(),
						'i' => array(),
					)
				),
				esc_html( CTC_CRE_NAME_SHORT )
			);

			?>

		</p>
	</div>
	<?php

}

add_action( 'admin_notices', 'ctc_cre_unsupported_admin_notice' );
