<?php
/**
 * WordPress Importer
 *
 * It can useful to modify data after importing an XML file with the WordPress Importer plugin.
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Admin
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*************************************************
 * AFTER IMPORT
 *************************************************/

/**
 * Set defaults after import
 *
 * A user may be importing old sample content or site export XML file while using up to date plugin.
 * This would mean the upgrade routine is not run because version in database is fine.
 *
 * Let's at least set defaults on custom fields to help make things smooth.
 * Later, if necessary, it may be useful to run entire upgrade routine if it is true that that will
 * not cause destruction if re-run.
 *
 * @since 1.0
 */
function ctc_cre_after_import() {

	// Set defaults for fields that did not always exist.
	ctc_cre_set_field_defaults();

}

add_action( 'import_end', 'ctc_cre_after_import' ); // WordPress Importer plugin hook.
