<?php
/**
 * Event Fields in Admin
 *
 * Fields, meta box and admin columns.
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Admin
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**********************************
 * META BOXES
 **********************************/

/**
 * Insert custom recurrence fields
 *
 * This will add fields for custom recurrence like bi-weekly, third Sunday of month, etc.
 *
 * @since  1.0
 * @param Array $fields Current fields to modify.
 * @return Array Original fields array with custom recurrence fields merged in after Recurrence field.
 */
function ctc_cre_insert_fields( $fields ) {

	// Only if Recurrence is supported by theme (no reason it shouldn't be).
	if ( ! ctc_field_supported( 'events', '_ctc_event_recurrence' ) ) {
		return $fields;
	}

	// Fields to insert.
	$recurrence_fields = array(

		// Recur Every X Weeks.
		'_ctc_event_recurrence_weekly_interval' => array(
			'name'              => _x( 'Recur Every', 'weeks', 'ctc-custom-recurring-events' ),
			'after_name'        => '', // (Optional), (Required), etc.
			'after_input'       => __( 'week(s)', 'ctc-custom-recurring-events' ), // text to show to right of input (fields: text, select, number, upload, url, date, time).
			'desc'              => '',
			'type'              => 'number', // text, textarea, checkbox, radio, select, number, upload, upload_textarea, url, date, time.
			'checkbox_label'    => '', // show text after checkbox.
			'options'           => array(), // array of keys/values for radio or select.
			'upload_button'     => '', // text for button that opens media frame.
			'upload_title'      => '', // title appearing at top of media frame.
			'upload_type'       => '', // optional type of media to filter by (image, audio, video, application/pdf).
			'default'           => '1', // value to pre-populate option with (before first save or on reset).
			'no_empty'          => true, // if user empties value, force default to be saved instead.
			'allow_html'        => false, // allow HTML to be used in the value (text, textarea).
			'attributes'        => array( // attr => value array (e.g. set min/max for number type).
				'min' => '1',
			),
			'class'             => 'ctmb-three-digits', // class(es) to add to input (try ctmb-medium, ctmb-small, ctmb-tiny).
			'field_attributes'  => array(), // attr => value array for field container.
			'field_class'       => '', // class(es) to add to field container.
			'custom_sanitize'   => '', // function to do additional sanitization.
			'custom_field'      => '', // function for custom display of field input.
			'visibility'        => array( // show this field only when other field(s) have certain values: array( array( 'field1' => 'value' ), array( 'field2' => array( 'value', '!=' ) ).
				'_ctc_event_recurrence' => 'weekly',
			),
		),

		// Recur Every X Months.
		'_ctc_event_recurrence_monthly_interval' => array(
			'name'              => _x( 'Recur Every', 'months', 'ctc-custom-recurring-events' ),
			'after_name'        => '', // (Optional), (Required), etc.
			'after_input'       => __( 'month(s)', 'ctc-custom-recurring-events' ), // text to show to right of input (fields: text, select, number, upload, url, date, time).
			'desc'              => '',
			'type'              => 'number', // text, textarea, checkbox, radio, select, number, upload, upload_textarea, url, date, time.
			'checkbox_label'    => '', // show text after checkbox.
			'options'           => array(), // array of keys/values for radio or select.
			'upload_button'     => '', // text for button that opens media frame.
			'upload_title'      => '', // title appearing at top of media frame.
			'upload_type'       => '', // optional type of media to filter by (image, audio, video, application/pdf).
			'default'           => '1', // value to pre-populate option with (before first save or on reset).
			'no_empty'          => true, // if user empties value, force default to be saved instead.
			'allow_html'        => false, // allow HTML to be used in the value (text, textarea).
			'attributes'        => array( // attr => value array (e.g. set min/max for number type).
				'min' => '1',
			),
			'class'             => 'ctmb-three-digits', // class(es) to add to input (try ctmb-medium, ctmb-small, ctmb-tiny).
			'field_attributes'  => array(), // attr => value array for field container.
			'field_class'       => '', // class(es) to add to field container.
			'custom_sanitize'   => '', // function to do additional sanitization.
			'custom_field'      => '', // function for custom display of field input.
			'visibility'        => array( // show this field only when other field(s) have certain values: array( array( 'field1' => 'value' ), array( 'field2' => array( 'value', '!=' ) ).
				'_ctc_event_recurrence' => 'monthly',
			),
		),

		// Recur Monthly Type.
		// 'day' can later trigger a new field for multiple days like Apple Calendar.
		'_ctc_event_recurrence_monthly_type' => array(
			'name'              => '',
			'after_name'        => '', // (Optional), (Required), etc.
			'after_input'       => '', // text to show to right of input (fields: text, select, number, upload, url, date, time).
			'desc'              => '',
			'type'              => 'radio', // text, textarea, checkbox, radio, select, number, upload, upload_textarea, url, date, time.
			'checkbox_label'    => '', // show text after checkbox.
			'options'           => array( // array of keys/values for radio or select.
				'day'  => __( 'On same day of the month', 'ctc-custom-recurring-events' ),
				'week' => __( 'On a specific week...', 'ctc-custom-recurring-events' ),
			),
			'upload_button'     => '', // text for button that opens media frame.
			'upload_title'      => '', // title appearing at top of media frame.
			'upload_type'       => '', // optional type of media to filter by (image, audio, video, application/pdf).
			'default'           => 'day', // value to pre-populate option with (before first save or on reset).
			'no_empty'          => true, // if user empties value, force default to be saved instead.
			'allow_html'        => false, // allow HTML to be used in the value (text, textarea).
			'attributes'        => array(), // attr => value array (e.g. set min/max for number type).
			'class'             => '', // class(es) to add to input (try ctmb-medium, ctmb-small, ctmb-tiny).
			'field_attributes'  => array(), // attr => value array for field container.
			'field_class'       => '', // class(es) to add to field container.
			'custom_sanitize'   => '', // function to do additional sanitization.
			'custom_field'      => '', // function for custom display of field input.
			'visibility'        => array( // show this field only when other field(s) have certain values: array( array( 'field1' => 'value' ), array( 'field2' => array( 'value', '!=' ) ).
				'_ctc_event_recurrence' => 'monthly',
			),
		),

		// Recur Monthly on Week.
		'_ctc_event_recurrence_monthly_week' => array(
			'name'              => '',
			'after_name'        => '', // (Optional), (Required), etc.
			'after_input'       => '', // text to show to right of input (fields: text, select, number, upload, url, date, time).
			'desc'              => __( 'Day of the week is the same as Start Date.', 'ctc-custom-recurring-events' ),
			'type'              => 'select', // text, textarea, checkbox, radio, select, number, upload, upload_textarea, url, date, time.
			'checkbox_label'    => '', // show text after checkbox.
			'options'           => array( // array of keys/values for radio or select.
				''     => __( 'Select a Week', 'ctc-custom-recurring-events' ),
				'1'    => _x( 'First', 'week of month', 'ctc-custom-recurring-events' ),
				'2'    => _x( 'Second', 'week of month', 'ctc-custom-recurring-events' ),
				'3'    => _x( 'Third', 'week of month', 'ctc-custom-recurring-events' ),
				'4'    => _x( 'Fourth', 'week of month', 'ctc-custom-recurring-events' ),
				'last' => _x( 'Last', 'week of month', 'ctc-custom-recurring-events' ),
			),
			'upload_button'     => '', // text for button that opens media frame.
			'upload_title'      => '', // title appearing at top of media frame.
			'upload_type'       => '', // optional type of media to filter by (image, audio, video, application/pdf).
			'default'           => '', // value to pre-populate option with (before first save or on reset).
			'no_empty'          => true, // if user empties value, force default to be saved instead.
			'allow_html'        => false, // allow HTML to be used in the value (text, textarea).
			'attributes'        => array(), // attr => value array (e.g. set min/max for number type).
			'class'             => '', // class(es) to add to input (try ctmb-medium, ctmb-small, ctmb-tiny).
			'field_attributes'  => array(), // attr => value array for field container.
			'field_class'       => '', // class(es) to add to field container.
			'custom_sanitize'   => '', // function to do additional sanitization.
			'custom_field'      => '', // function for custom display of field input.
			'visibility'        => array( // show this field only when other field(s) have certain values: array( array( 'field1' => 'value' ), array( 'field2' => array( 'value', '!=' ) ).
				'_ctc_event_recurrence'              => 'monthly', // and...
				'_ctc_event_recurrence_monthly_type' => 'week',
			),
		),

	);

	/* If add more fields, update support.php to auto-support them. */

	// Make the fields being inserted filterable.
	$recurrence_fields = apply_filters( 'ctc_cre_fields', $recurrence_fields );

	// Insert custom fields after existing "Recurrence" field.
	$modified_fields = ctc_cre_array_merge_after_key( $fields, $recurrence_fields, '_ctc_event_recurrence' );

	return $modified_fields;

}

add_filter( 'ctmb_fields-ctc_event_date', 'ctc_cre_insert_fields' );

/**********************************
 * AFTER SAVING
 **********************************/

/**
 * Recur Monthly Type Correction
 *
 * If "On a specific week" is chosen but no week is chosen, fall back to "On same day of the month"
 * Similarly, if "On same day of month" is chosen, set week to nothing
 *
 * @since 1.0
 * @param int $post_id Post ID.
 */
function ctc_cre_correct_monthly_type( $post_id ) {

	// Get values.
	$monthly_type = get_post_meta( $post_id, '_ctc_event_recurrence_monthly_type', true );
	$monthly_week = get_post_meta( $post_id, '_ctc_event_recurrence_monthly_week', true );

	// If type is week and week is empty, set type to day.
	if ( 'week' === $monthly_type && ! $monthly_week ) {
		update_post_meta( $post_id, '_ctc_event_recurrence_monthly_type', 'day' );
	}

	// If type is day and week is not empty, empty it (so user is forced to choose when going back).
	elseif ( 'day' === $monthly_type && $monthly_week ) {
		update_post_meta( $post_id, '_ctc_event_recurrence_monthly_week', '' );
	}

}

add_action( 'ctc_after_save_event', 'ctc_cre_correct_monthly_type' ); // run after event post is saved.

/**********************************
 * ADMIN COLUMNS
 **********************************/

/**
 * Filter recurrence note to consider custom recurrence
 *
 * @since 1.0
 * @param string $note Recurrence note.
 * @param array $data Post data, recurrence, and other data useful for output.
 * @return string New recurrence note.
 */
function ctc_cre_recurrence_note( $note, $data ) {

	// Get given data.
	$post = $data['post'];
	$recurrence = $data['recurrence'];
	$recurrence_end_date = $data['recurrence_end_date'];
	$recurrence_end_date_localized = $data['recurrence_end_date_localized'];

	// Get custom recurrence data.
	$weekly_interval = get_post_meta( $post->ID , '_ctc_event_recurrence_weekly_interval' , true );
	$monthly_interval = get_post_meta( $post->ID , '_ctc_event_recurrence_monthly_interval' , true );
	$monthly_type = get_post_meta( $post->ID , '_ctc_event_recurrence_monthly_type' , true );
	$monthly_week = get_post_meta( $post->ID , '_ctc_event_recurrence_monthly_week' , true );

	// Get day of week for start date.
	$start_date = get_post_meta( $post->ID , '_ctc_event_start_date' , true );
	$start_day_of_week = ! empty( $start_date ) ? date_i18n( 'l', strtotime( $start_date ) ) : '';

	// Words for week of month.
	$monthly_week_word = '';
	if ( $monthly_week ) {

		$monthly_week_words = array(
			'1'    => esc_html_x( 'first', 'week of month', 'ctc-custom-recurring-events' ),
			'2'    => esc_html_x( 'second', 'week of month', 'ctc-custom-recurring-events' ),
			'3'    => esc_html_x( 'third', 'week of month', 'ctc-custom-recurring-events' ),
			'4'    => esc_html_x( 'fourth', 'week of month', 'ctc-custom-recurring-events' ),
			'last' => esc_html_x( 'last', 'week of month', 'ctc-custom-recurring-events' ),
		);

		$monthly_week_word = $monthly_week_words[ $monthly_week ];

	}

	// Frequency.
	switch ( $recurrence ) {

		case 'weekly':

			if ( $recurrence_end_date ) {

				/* translators: %1$s is interval, %2$s is recurrence end date */
				$note = sprintf(
					/* translators: %1$s is weekly interval, %2$s is recurrence end date localized */
					_n(
						'Every week until %2$s',
						'Every %1$s weeks until %2$s',
						$weekly_interval,
						'ctc-custom-recurring-events'
					),
					$weekly_interval,
					$recurrence_end_date_localized
				);

			} else {

				/* translators: %1$s is interval */
				$note = sprintf(
					/* translators: %1$s is weekly interval */
					_n(
						'Every week',
						'Every %1$s weeks',
						$weekly_interval,
						'ctc-custom-recurring-events'
					),
					$weekly_interval
				);

			}

			break;

		case 'monthly':

			// On specific week.
			if ( 'week' == $monthly_type && $start_day_of_week ) { // only if start date is present.

				// Has recurrence end date.
				if ( $recurrence_end_date ) {

					$note = sprintf(
						/* translators: %1$s is interval, %2$s is week of month, %3$s is day of week, %4$s is recurrence end date */
						_n(
							'Every month (%2$s %3$s) until %4$s',
							'Every %1$s months (%2$s %3$s) until %4$s',
							$monthly_interval,
							'ctc-custom-recurring-events'
						),
						$monthly_interval,
						$monthly_week_word,
						$start_day_of_week,
						$recurrence_end_date_localized
					);

				}

				// No recurrence end date.
				else {

					/* translators: %1$s is interval, %2$s is week of month, %3$s is day of week */
					$note = sprintf(
						_n(
							'Every month (%2$s %3$s)',
							'Every %1$s months (%2$s %3$s)',
							$monthly_interval,
							'ctc-custom-recurring-events'
						),
						$monthly_interval,
						$monthly_week_word,
						$start_day_of_week
					);

				}

			// On same day of month.
			} else {

				// Has recurrence end date.
				if ( $recurrence_end_date ) {

					$note = sprintf(
						/* translators: %1$s is interval, %2$s is recurrence end date */
						_n(
							'Every month until %2$s',
							'Every %1$s months until %2$s',
							$monthly_interval,
							'ctc-custom-recurring-events'
						),
						$monthly_interval,
						$recurrence_end_date_localized
					);

				}

				// No recurrence end date.
				else {

					$note = sprintf(
						/* translators: %1$s is interval */
						_n(
							'Every month',
							'Every %1$s months',
							$monthly_interval,
							'ctc-custom-recurring-events'
						),
						$monthly_interval
					);

				}

			}

			break;

		case 'yearly':

			if ( $recurrence_end_date ) {

				$note = sprintf(
					/* translators: %1$s is recurrence end date */
					__( 'Every year until %1$s', 'ctc-custom-recurring-events' ),
					$recurrence_end_date_localized
				);

			} else {
				$note = __( 'Every year', 'ctc-custom-recurring-events' );
			}

			break;

	}

	// Translation security.
	$note = esc_html( $note );

	return $note;

}

add_filter( 'ctc_event_columns_recurrence_note', 'ctc_cre_recurrence_note', 10, 2 );

/**********************************
 * DATABASE UPGRADES
 **********************************/

/**
 * Set Recurrence Field Defaults (All Events)
 *
 * Version 1.0 introduced the recurrence fields.
 * This fills in defaults for these new fields.
 *
 * This is run by the database upgrader.
 * See ctc_cre_upgrade_1_0() in includes/upgrade.php
 *
 * NOTE: This can be modified in future to accommodate other new fields.
 *
 * @since 1.0
 */
function ctc_cre_set_field_defaults() {

	// Select all events to check/update.
	$posts = get_posts( array(
		'post_type'   => 'ctc_event',
		'post_status' => 'publish,pending,draft,auto-draft,future,private,inherit,trash', // all to be safe.
		'numberposts' => -1, // no limit.
	) );

	// Loop each post to update fields.
	foreach ( $posts as $post ) {

		// Get current values.
		$recurrence_weekly_interval = get_post_meta( $post->ID, '_ctc_event_recurrence_weekly_interval', true );
		$recurrence_monthly_interval = get_post_meta( $post->ID, '_ctc_event_recurrence_monthly_interval', true );
		$recurrence_monthly_type = get_post_meta( $post->ID, '_ctc_event_recurrence_monthly_type', true );
		$recurrence_monthly_week = get_post_meta( $post->ID, '_ctc_event_recurrence_monthly_week', true );

		// Set defaults for the custom recurrence fields.
		// These were introduced in 1.0.
		if ( ! $recurrence_weekly_interval ) {
			update_post_meta( $post->ID, '_ctc_event_recurrence_weekly_interval', '1' );
		}
		if ( ! $recurrence_monthly_interval ) {
			update_post_meta( $post->ID, '_ctc_event_recurrence_monthly_interval', '1' );
		}
		if ( ! $recurrence_monthly_type ) {
			update_post_meta( $post->ID, '_ctc_event_recurrence_monthly_type', 'day' );
		}
		if ( ! $recurrence_monthly_week ) {
			update_post_meta( $post->ID, '_ctc_event_recurrence_monthly_week', '' );
		}

	}

}
