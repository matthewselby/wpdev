<?php
/**
 * Feature Support
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Functions
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/****************************************
 * THEME SUPPORT
 ****************************************

/**
 * Add support for custom recurrence fields
 *
 * If the basic Recurrence field is supported, add support for custom recurrence fields.
 * Theme authors should never need to specify support for these add-on fields. It's automatic.
 *
 * @since 1.0
 */
function ctc_cre_set_fields_theme_support() {

	// Get event fields that are supported.
	$supported_fields = ctc_get_theme_support( 'ctc-events', 'fields' );

	// Only if specific fields are supported.
	// Because, if no fields specified, all are automatically supported (including the custom recurrence fields).
	if ( ! isset( $supported_fields ) ) {
		return;
	}

	// Only if Recurrence field supported.
	if ( ! ctc_field_supported( 'events', '_ctc_event_recurrence' ) ) {
		return;
	}

	// Custom recurrence fields to be supported.
	$fields = array(
		'_ctc_event_recurrence_weekly_interval',
		'_ctc_event_recurrence_monthly_interval',
		'_ctc_event_recurrence_monthly_type',
		'_ctc_event_recurrence_monthly_week',
	);

	// Ensure custom recurrence fields are supported.
	$modified_fields = (array) $supported_fields; // ensure value provided for fields is array.
	$modified_fields = array_merge( $modified_fields, $fields ); // add them.
	$modified_fields = array_unique( $modified_fields ); // no duplicates.

	// Modify theme support data.
	$events_support = ctc_get_theme_support( 'ctc-events' );
	$events_support_modified = $events_support;
	$events_support_modified['fields'] = $modified_fields;

	// Update theme support.
	remove_theme_support( 'ctc-events' );
	add_theme_support( 'ctc-events', $events_support_modified );

}

add_action( 'init', 'ctc_cre_set_fields_theme_support', 2 ); // init 2 is right after ctc_set_default_theme_support in CTC.
