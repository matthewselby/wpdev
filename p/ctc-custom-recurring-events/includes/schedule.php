<?php
/**
 * Scheduled Functions
 *
 * @package    CTC_Custom_Recurring_Events
 * @subpackage Functions
 * @copyright  Copyright (c) 2014 - 2017, churchthemes.com
 * @link       https://churchthemes.com/plugins/custom-recurring-events
 * @license    GPLv2 or later
 * @since      1.0
 */

// No direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*************************************************
 * RECURRING EVENTS
 *************************************************/

/**
 * Handle custom recurrence
 *
 * This filters CT_Recurrence arguments in CTC's recurring events date updater
 *
 * @since 1.0
 * @param Array $args Arguments for CT_Recurrence calc_next_future_date().
 * @param Array $post Event post.
 * @return Array Modified arguments having consideration for custom recurring event fields.
 */
function ctc_cre_custom_recurrence_args( $args, $post ) {

	// Get basic recurrence.
	$recurrence = get_post_meta( $post->ID, '_ctc_event_recurrence', true );

	// Get custom recurrence.
	$recurrence_weekly_interval = get_post_meta( $post->ID, '_ctc_event_recurrence_weekly_interval', true );
	$recurrence_monthly_interval = get_post_meta( $post->ID, '_ctc_event_recurrence_monthly_interval', true );
	$recurrence_monthly_type = get_post_meta( $post->ID, '_ctc_event_recurrence_monthly_type', true );
	$recurrence_monthly_week = get_post_meta( $post->ID, '_ctc_event_recurrence_monthly_week', true );

	// Interval.
	$interval = 1;
	if ( 'weekly' === $recurrence ) {
		$interval = $recurrence_weekly_interval;
	} elseif ( 'monthly' === $recurrence ) {
		$interval = $recurrence_monthly_interval;
	}

	// Merge arguments in for custom recurrence.
	$modified_args = array_merge( $args, array(
		'interval'      => $interval, // every 1, 2 or 3, etc. weeks, months or years.
		'monthly_type'  => $recurrence_monthly_type, // day (same day of month) or week (on a specific week); if recurrence is monthly (day is default).
		'monthly_week'  => $recurrence_monthly_week, // 1 - 4 or 'last'; if recurrence is monthly and monthly_type is 'week'.
	) );

	return $modified_args;

}

add_filter( 'ctc_event_recurrence_args', 'ctc_cre_custom_recurrence_args', 10, 2 );
